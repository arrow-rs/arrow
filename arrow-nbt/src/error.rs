use thiserror::Error;

#[derive(Error, Debug)]
pub enum NbtError {
    #[error("Pushed type mismatches list.")]
    ListTypeMismatch,
}
