use error::NbtError;

pub mod error;

#[derive(Debug)]
pub struct NbtTag {
    name: String,
    ty: NbtType,
}

impl NbtTag {
    pub fn encode(&self) -> Vec<u8> {
        let mut out = vec![self.ty.id()];
        let len = self.name.len() as u16;
        out.extend_from_slice(&len.to_be_bytes());
        out.extend_from_slice(self.name.as_bytes());
        out.append(&mut self.ty.encode_payload());

        out
    }
}

#[derive(Debug)]
pub struct NbtCompound(Vec<NbtTag>);

impl Default for NbtCompound {
    fn default() -> Self {
        Self::new()
    }
}

impl NbtCompound {
    pub fn new() -> Self {
        Self(vec![])
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn capacity(&self) -> usize {
        self.0.capacity()
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    pub fn push(&mut self, value: NbtTag) {
        self.0.push(value)
    }
}

#[derive(Debug)]
pub struct NbtList(Vec<NbtType>);

impl Default for NbtList {
    fn default() -> Self {
        Self::new()
    }
}

impl NbtList {
    pub fn new() -> Self {
        Self(vec![])
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn capacity(&self) -> usize {
        self.0.capacity()
    }

    pub fn push(&mut self, value: NbtType) -> Result<(), NbtError> {
        if !self.is_empty() && value.id() != self.0[0].id() {
            return Err(NbtError::ListTypeMismatch);
        }

        self.0.push(value);

        Ok(())
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    pub fn encode(&self) -> Vec<u8> {
        self.0.iter().flat_map(NbtType::encode_payload).collect()
    }
}

#[derive(Debug)]
pub enum NbtType {
    End,
    Byte(i8),
    Short(i16),
    Int(i32),
    Long(i64),
    Float(f32),
    Double(f64),
    ByteArray(Vec<i8>),
    String(String),
    List(NbtList),
    Compound(),
    IntArray(Vec<i32>),
    LongArray(Vec<i64>),
}

impl NbtType {
    pub fn id(&self) -> u8 {
        match self {
            NbtType::End => 0,
            NbtType::Byte(_) => 1,
            NbtType::Short(_) => 2,
            NbtType::Int(_) => 3,
            NbtType::Long(_) => 4,
            NbtType::Float(_) => 5,
            NbtType::Double(_) => 6,
            NbtType::ByteArray(_) => 7,
            NbtType::String(_) => 8,
            NbtType::List(_) => 9,
            NbtType::Compound() => 10,
            NbtType::IntArray(_) => 11,
            NbtType::LongArray(_) => 12,
        }
    }

    pub fn encode(&self) -> Vec<u8> {
        let mut out = vec![self.id()];
        out.append(&mut self.encode_payload());

        out
    }

    fn encode_payload(&self) -> Vec<u8> {
        match self {
            NbtType::End => vec![],
            NbtType::Byte(b) => vec![*b as u8],
            NbtType::Short(s) => s.to_be_bytes().to_vec(),
            NbtType::Int(i) => i.to_be_bytes().to_vec(),
            NbtType::Long(l) => l.to_be_bytes().to_vec(),
            NbtType::Float(f) => f.to_be_bytes().to_vec(),
            NbtType::Double(d) => d.to_be_bytes().to_vec(),
            NbtType::ByteArray(a) => {
                let mut out = (a.len() as i32).to_be_bytes().to_vec();
                out.extend(a.iter().map(|i| *i as u8));

                out
            }
            NbtType::String(s) => {
                let mut out = (s.len() as i32).to_be_bytes().to_vec();
                out.extend_from_slice(s.as_bytes());

                out
            }
            NbtType::List(_) => todo!(),
            NbtType::Compound() => todo!(),
            NbtType::IntArray(a) => {
                let mut out = (a.len() as i32).to_be_bytes().to_vec();
                out.extend(a.iter().flat_map(|i| i.to_be_bytes()));

                out
            }
            NbtType::LongArray(a) => {
                let mut out = (a.len() as i32).to_be_bytes().to_vec();
                out.extend(a.iter().flat_map(|i| i.to_be_bytes()));

                out
            }
        }
    }
}
