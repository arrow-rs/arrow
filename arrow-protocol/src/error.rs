use thiserror::Error;

use crate::protocol::State;

pub(crate) type Res<T> = Result<T, ProtocolError>;

#[derive(Debug, Error)]
pub enum ProtocolError {
    #[error("Unexpected eof.")]
    UnexpectedEof,
    #[error("VarInt too long.")]
    VarIntTooLong,
    #[error("Invalid utf8.")]
    InvalidUtf8,
    #[error("Unknown packet id: {0} in state {1:?}.")]
    UnknownId(i32, State),
    #[error("Unsupported protocol version: {0}")]
    UnsupportedProtocolVersion(i32),
    #[error("No protocol version given.")]
    NoProtocolVersionGiven,
    #[error("Invalid json: {0}")]
    InvaidJson(serde_json::Error),
    #[error("Unknown variant {0} for enum {1}")]
    UnknownVariant(String, &'static str),
}
