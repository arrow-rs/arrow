use serde::{Deserialize, Serialize};
use serde_json::{from_str, to_string};

use crate::{error::ProtocolError, types};

#[derive(Debug)]
pub struct Chat {
    pub text: Component,
}

impl Chat {
    pub fn new(text: Component) -> Self {
        Self { text }
    }

    pub fn json(&self) -> Result<String, serde_json::Error> {
        to_string(&self.text)
    }

    pub fn parse(s: &str) -> Result<Self, serde_json::Error> {
        Ok(Self { text: from_str(s)? })
    }
}

impl crate::types::Serialize for Chat {
    fn serialize(&self, buf: &mut bytes::BytesMut) {
        types::Serialize::serialize(&self.json().unwrap(), buf)
    }

    fn deserialize<B: bytes::Buf>(buf: &mut B) -> crate::error::Res<Self> {
        let str: String = types::Serialize::deserialize(buf)?;

        Self::parse(&str).map_err(ProtocolError::InvaidJson)
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum Component {
    Object(Box<Object>),
    Array(Vec<Component>),
    Primitive(String),
}

#[serde_with::skip_serializing_none]
#[derive(Default, Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Object {
    pub bold: Option<bool>,
    pub italic: Option<bool>,
    pub underlined: Option<bool>,
    pub strikethrough: Option<bool>,
    pub obfuscated: Option<bool>,
    pub font: Option<Font>,
    pub color: Option<String>,
    pub insertion: Option<String>,
    pub click_event: Option<ClickEvent>,
    pub hover_event: Option<HoverEvent>,
    pub extra: Option<Vec<Component>>,
    pub text: Option<String>,
    pub translation: Option<String>,
    pub with: Option<Vec<Component>>,
    pub keybind: Option<String>,
    pub score: Option<Score>,
    pub selector: Option<String>,
}

#[derive(Default)]
pub struct ObjectBuilder {
    bold: Option<bool>,
    italic: Option<bool>,
    underlined: Option<bool>,
    strikethrough: Option<bool>,
    obfuscated: Option<bool>,
    font: Option<Font>,
    color: Option<String>,
    insertion: Option<String>,
    click_event: Option<ClickEvent>,
    hover_event: Option<HoverEvent>,
    extra: Option<Vec<Component>>,
    text: Option<String>,
    translation: Option<String>,
    with: Option<Vec<Component>>,
    keybind: Option<String>,
    score: Option<Score>,
    selector: Option<String>,
}

impl ObjectBuilder {
    pub fn build(self) -> Object {
        Object {
            bold: self.bold,
            italic: self.italic,
            underlined: self.underlined,
            strikethrough: self.strikethrough,
            obfuscated: self.obfuscated,
            font: self.font,
            color: self.color,
            insertion: self.insertion,
            click_event: self.click_event,
            hover_event: self.hover_event,
            extra: self.extra,
            text: self.text,
            translation: self.translation,
            with: self.with,
            keybind: self.keybind,
            score: self.score,
            selector: self.selector,
        }
    }

    pub fn bold(mut self, bold: bool) -> Self {
        self.bold = Some(bold);
        self
    }

    pub fn italic(mut self, italic: bool) -> Self {
        self.italic = Some(italic);
        self
    }

    pub fn underlined(mut self, underlined: bool) -> Self {
        self.underlined = Some(underlined);
        self
    }

    pub fn strikethrough(mut self, strikethrough: bool) -> Self {
        self.strikethrough = Some(strikethrough);
        self
    }

    pub fn obfuscated(mut self, obfuscated: bool) -> Self {
        self.obfuscated = Some(obfuscated);
        self
    }

    pub fn font(mut self, font: Font) -> Self {
        self.font = Some(font);
        self
    }

    pub fn color(mut self, color: String) -> Self {
        self.color = Some(color);
        self
    }

    pub fn insertion(mut self, insertion: String) -> Self {
        self.insertion = Some(insertion);
        self
    }

    pub fn click_event(mut self, click_event: ClickEvent) -> Self {
        self.click_event = Some(click_event);
        self
    }

    pub fn hover_event(mut self, hover_event: HoverEvent) -> Self {
        self.hover_event = Some(hover_event);
        self
    }

    pub fn extra(mut self, component: Component) -> Self {
        if self.extra.is_none() {
            self.extra = Some(vec![]);
        }

        self.extra.as_mut().unwrap().push(component);

        self
    }

    pub fn text(mut self, text: String) -> Self {
        self.text = Some(text);
        self
    }

    pub fn translation(mut self, translation: String) -> Self {
        self.translation = Some(translation);
        self
    }

    pub fn with(mut self, component: Component) -> Self {
        if self.with.is_none() {
            self.with = Some(vec![]);
        }

        self.with.as_mut().unwrap().push(component);

        self
    }

    pub fn keybind(mut self, keybind: String) -> Self {
        self.keybind = Some(keybind);
        self
    }

    pub fn score(mut self, score: Score) -> Self {
        self.score = Some(score);
        self
    }

    pub fn selector(mut self, selector: String) -> Self {
        self.selector = Some(selector);
        self
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum Font {
    #[serde(rename = "minecraft:uniform")]
    Uniform,
    #[serde(rename = "minecraft:alt")]
    Alt,
    #[serde(rename = "minecraft:default")]
    Default,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct ClickEvent {
    action: ClickAction,
    value: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum ClickAction {
    #[serde(rename = "open_url")]
    OpenUrl,
    #[serde(rename = "run_command")]
    RunCommand,
    #[serde(rename = "suggest_command")]
    SuggestCommand,
    #[serde(rename = "change_page")]
    ChangePage,
    #[serde(rename = "copy_to_clipboard")]
    CopyToClipboard,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct HoverEvent {
    action: HoverAction,
    value: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum HoverAction {
    #[serde(rename = "show_text")]
    ShowText,
    #[serde(rename = "show_item")]
    ShowItem,
    #[serde(rename = "show_entity")]
    ShowEntity,
    #[serde(rename = "show_achievement")]
    ShowAchievement,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum Score {
    ToClient {
        name: String,
        objective: String,
        value: i32,
    },
    ToServer {
        name: String,
        objective: String,
    },
}

#[test]
fn test_json() {
    use serde_json::to_string_pretty;

    let object = ObjectBuilder::default()
        .bold(true)
        .italic(true)
        .underlined(true)
        .strikethrough(true)
        .obfuscated(true)
        .font(Font::Alt)
        .color("#f00baa".to_string())
        .insertion("Hello".to_string())
        .text("World".to_string())
        .translation("Xd".to_string())
        .keybind("Keybind".to_string())
        .selector("Selector".to_string())
        .click_event(ClickEvent {
            action: ClickAction::OpenUrl,
            value: "Foobar".to_string(),
        })
        .hover_event(HoverEvent {
            action: HoverAction::ShowText,
            value: "123".to_string(),
        })
        .build();

    let str = to_string(&object).unwrap();
    let object2: Object = from_str(&str).unwrap();
    assert!(object == object2);

    println!("{}", to_string_pretty(&object).unwrap());
}
