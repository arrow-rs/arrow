use crate::types::VarInt;

packet! {
    0x0 => Handshake {
        version: VarInt,
        addr: String,
        port: u16,
        next_state: VarInt
    }
}
