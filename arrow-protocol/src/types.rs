use arrow_nbt::{NbtTag, NbtType};
use bytes::{Buf, BufMut, BytesMut};
use uuid::Uuid;

use crate::error::{ProtocolError, Res};

pub trait Serialize: Sized {
    fn serialize(&self, buf: &mut BytesMut);
    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self>;
}

impl Serialize for bool {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.put_u8(*self as u8)
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        if buf.remaining() < 1 {
            Err(ProtocolError::UnexpectedEof)
        } else {
            Ok(buf.get_u8() != 0)
        }
    }
}

impl Serialize for u8 {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.put_u8(*self);
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        if buf.remaining() < 1 {
            Err(ProtocolError::UnexpectedEof)
        } else {
            Ok(buf.get_u8())
        }
    }
}

impl Serialize for i8 {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.put_i8(*self);
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        if buf.remaining() < 1 {
            Err(ProtocolError::UnexpectedEof)
        } else {
            Ok(buf.get_i8())
        }
    }
}

impl Serialize for u16 {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.put_u16(*self);
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        if buf.remaining() < 2 {
            Err(ProtocolError::UnexpectedEof)
        } else {
            Ok(buf.get_u16())
        }
    }
}

impl Serialize for i16 {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.put_i16(*self);
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        if buf.remaining() < 2 {
            Err(ProtocolError::UnexpectedEof)
        } else {
            Ok(buf.get_i16())
        }
    }
}

impl Serialize for u32 {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.put_u32(*self);
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        if buf.remaining() < 4 {
            Err(ProtocolError::UnexpectedEof)
        } else {
            Ok(buf.get_u32())
        }
    }
}

impl Serialize for i32 {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.put_i32(*self);
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        if buf.remaining() < 4 {
            Err(ProtocolError::UnexpectedEof)
        } else {
            Ok(buf.get_i32())
        }
    }
}

impl Serialize for u64 {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.put_u64(*self);
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        if buf.remaining() < 8 {
            Err(ProtocolError::UnexpectedEof)
        } else {
            Ok(buf.get_u64())
        }
    }
}

impl Serialize for i64 {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.put_i64(*self);
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        if buf.remaining() < 8 {
            Err(ProtocolError::UnexpectedEof)
        } else {
            Ok(buf.get_i64())
        }
    }
}

impl Serialize for f32 {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.put_f32(*self);
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        if buf.remaining() < 4 {
            Err(ProtocolError::UnexpectedEof)
        } else {
            Ok(buf.get_f32())
        }
    }
}

impl Serialize for f64 {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.put_f64(*self);
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        if buf.remaining() < 8 {
            Err(ProtocolError::UnexpectedEof)
        } else {
            Ok(buf.get_f64())
        }
    }
}

impl Serialize for Uuid {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.put_slice(self.as_bytes());
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        if buf.remaining() < 16 {
            Err(ProtocolError::UnexpectedEof)
        } else {
            Ok(Uuid::from_u128(buf.get_u128()))
        }
    }
}

pub struct VarInt(pub i32, pub u8);

impl std::fmt::Debug for VarInt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", self.0))
    }
}

impl Serialize for VarInt {
    fn serialize(&self, buf: &mut BytesMut) {
        let mut value = self.0 as u32;

        loop {
            if (value & !0x7F) == 0 {
                buf.put_u8(value as u8);
                break;
            }

            buf.put_u8((value as u8 & 0x7F) | 0x80);
            value >>= 7;
        }
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        let mut value = 0;
        let mut len = 0;

        while {
            if buf.remaining() == 0 {
                return Err(ProtocolError::UnexpectedEof);
            }
            let b = buf.get_u8() as i32;
            value |= (b & 0x7f) << (len * 7);

            len += 1;

            if len >= 5 {
                panic!()
            };

            (b & 0x80) != 0
        } {}

        Ok(Self(value, len))
    }
}

impl Serialize for String {
    fn serialize(&self, buf: &mut BytesMut) {
        VarInt(self.len() as i32, 0).serialize(buf);
        buf.put(self.as_bytes());
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        let len = VarInt::deserialize(buf)?.0 as usize;

        if buf.remaining() < len {
            Err(ProtocolError::UnexpectedEof)
        } else {
            let bytes = buf.copy_to_bytes(len);
            let string =
                String::from_utf8(bytes.to_vec()).map_err(|_| ProtocolError::InvalidUtf8)?;

            Ok(string)
        }
    }
}

pub struct NoLengthPrefixVec<T>(pub Vec<T>);

impl<T: std::fmt::Debug> std::fmt::Debug for NoLengthPrefixVec<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let vec = &self.0;

        f.write_fmt(format_args!("{vec:?}"))
    }
}

impl Serialize for NoLengthPrefixVec<u8> {
    fn serialize(&self, buf: &mut BytesMut) {
        for i in &self.0 {
            i.serialize(buf);
        }
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        let mut vec = vec![0; buf.remaining()];
        buf.copy_to_slice(&mut vec);

        Ok(Self(vec))
    }
}

impl<T: Serialize> Serialize for Box<T> {
    fn serialize(&self, buf: &mut BytesMut) {
        self.as_ref().serialize(buf)
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        Ok(Box::new(T::deserialize(buf)?))
    }
}

impl<T: Serialize> Serialize for Vec<T> {
    fn serialize(&self, buf: &mut BytesMut) {
        VarInt(buf.len() as i32, 0).serialize(buf);
        for i in self {
            i.serialize(buf);
        }
    }

    fn deserialize<B: Buf>(buf: &mut B) -> Res<Self> {
        let len = VarInt::deserialize(buf)?.0 as usize;

        if buf.remaining() < len {
            Err(ProtocolError::UnexpectedEof)
        } else {
            let mut vec = Vec::with_capacity(len);

            for _ in 0..len {
                vec.push(T::deserialize(buf)?);
            }

            Ok(vec)
        }
    }
}

impl Serialize for NbtTag {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.extend_from_slice(&self.encode());
    }

    fn deserialize<B: Buf>(_buf: &mut B) -> Res<Self> {
        todo!()
    }
}

impl Serialize for NbtType {
    fn serialize(&self, buf: &mut BytesMut) {
        buf.extend_from_slice(&self.encode())
    }

    fn deserialize<B: Buf>(_buf: &mut B) -> Res<Self> {
        todo!()
    }
}
