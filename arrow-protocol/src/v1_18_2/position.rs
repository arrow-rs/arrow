use crate::types::Serialize;

#[derive(Debug)]
pub struct Position {
    pub x: u32,
    pub y: u16,
    pub z: u32,
}

impl Serialize for Position {
    fn serialize(&self, buf: &mut bytes::BytesMut) {
        let x = self.x.to_be() as u64;
        let y = self.y.to_be() as u64;
        let z = self.z.to_be() as u64;

        let pos = ((x & 0x3ffffff) << 38) | ((z & 0x3ffffff) << 12) | y & 0xfff;

        pos.serialize(buf);
    }

    fn deserialize<B: bytes::Buf>(buf: &mut B) -> crate::error::Res<Self> {
        let pos = u64::deserialize(buf)?;

        let x = (pos >> 38) as u32;
        let y = (pos & 0xfff) as u16;
        let z = ((pos >> 12) & 0x3ffffff) as u32;

        Ok(Self { x, y, z })
    }
}
