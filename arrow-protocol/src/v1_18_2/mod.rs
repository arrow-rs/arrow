use arrow_nbt::NbtType;
use uuid::Uuid;

use self::ids::EntityType;
use crate::{
    chat::Chat,
    error::ProtocolError,
    types::{Serialize, VarInt},
    v1_18_2::position::Position,
};

pub mod ids;
pub mod position;

protocol_state! {
    V1_18_2
    clientbound {
        0x0 => SpawnEntity {
            eid: VarInt,
            uuid: Uuid,
            ty: EntityType,
            x: f64,
            y: f64,
            z: f64,
            pitch: u8,
            yaw: u8,
            data: i32,
            velocity_x: i16,
            velocity_y: i16,
            velocity_z: i16
        },
        0x01 => SpawnExperienceOrb {
            eid: VarInt,
            x: f64,
            y: f64,
            z: f64,
            count: i16
        },
        0x02 => SpawnLivingEntity {
            eid: VarInt,
            uuid: Uuid,
            ty: EntityType,
            x: f64,
            y: f64,
            z: f64,
            yaw: u8,
            pitch: u8,
            head_yaw: u8,
            velocity_x: i16,
            velocity_y: i16,
            velocity_z: i16
        },
        0x03 => SpawnPainting {
            eid: VarInt,
            uuid: Uuid,
            motive: PaintingMotive,
            location: Position,
            direction: PaintingDirection
        },
        0x04 => SpawnPlayer {
            eid: VarInt,
            uuid: Uuid,
            x: f64,
            y: f64,
            z: f64,
            yaw: u8,
            pitch: u8
        },
        0x05 => SculkVibrationSignal {
            source_pos: Position,
            destination: SculkVibrationDestination,
            arrival_ticks: VarInt
        },
        0x06 => EntityAnimation {
            eid: VarInt,
            animation: EntityAnimation
        },
        0x07 => Statistics {
            statistics: Vec<Statistic>
        },
        0x08 => AcknowledgePlayerDigging {
            location: Position,
            block_state_id: VarInt,
            status: DiggingStatus,
            successful: bool
        },
        0x09 => BlockBreakAnimation {
            eid: VarInt,
            location: Position,
            stage: i8
        },
        0x0a => BlockEntityData {
            location: Position,
            ty: VarInt,
            data: NbtType
        },
        0x0b => BlockAction {
            location: Position,
            action_id: u8,
            param: u8,
            block_ty: VarInt
        },
        0x0c => BlockChange {
            location: Position,
            block_id: VarInt
        },
        0x0d => BossBar {
            uuid: Uuid,
            action: BossBarAction
        },
        0x0e => ServerDifficulty {
            difficulty: Difficulty,
            locked: bool
        },
        0x0f => ChatMessage {
            data: Box<Chat>,
            position: MessagePosition,
            sender: Uuid
        },
        0x10 => ClearTitles {
            reset: bool
        },
        0x11 => TabComplete {
            id: VarInt,
            start: VarInt,
            len: VarInt,
            matches: Vec<TabMatch>
        },
        0x12 => DeclareCommands {
            // TODO: Implement node parsers
            nodes: Vec<Node>,
            root_index: VarInt
        },
        0x13 => CloseWindow {
            id: u8
        },
        0x14 => WindowItems {
            id: u8,
            state_id: VarInt,
            slots: Vec<compile_error!("Slots not implemented")>,
            carried: compile_error!("Slots not implemented")
        },
        0x15 => WindowProperty {
            id: u8,
            property: i16,
            value: i16
        },
        0x16 => SetSlot {
            id: u8,
            state_id: VarInt,
            slot_num: i16,
            slot: compile_error!("Slots not implemented")
        },
        0x17 => SetCooldown {
            id: VarInt,
            cooldown: VarInt
        },
        0x18 => PluginMessage {
            channel: String,
            data: Vec<u8>
        }
    },
    serverbound {

    }
}

protocol_enum! {
    enum PaintingMotive: VarInt {
        Kebab = 0,
        Aztec = 1,
        Alban = 2,
        Aztec2 = 3,
        Bomb = 4,
        Plant = 5,
        Wasteland = 6,
        Pool = 7,
        Courbet = 8,
        Sea = 9,
        Sunset = 10,
        Creebet = 11,
        Wanderer = 12,
        Graham = 13,
        Match = 14,
        Bust = 15,
        Stage = 16,
        Void = 17,
        SkullAndRoses = 18,
        Wither = 19,
        Fighters = 20,
        Pointer = 21,
        Pigscene = 22,
        BurningSkull = 23,
        Skeleton = 24,
        DonkeyKong = 25
    }
}

impl PaintingMotive {
    pub fn width(&self) -> u8 {
        use PaintingMotive::*;

        match self {
            Kebab | Aztec | Alban | Aztec2 | Bomb | Plant | Wasteland | Wanderer | Graham => 16,
            Pool | Courbet | Sea | Sunset | Creebet | Match | Bust | Stage | Void
            | SkullAndRoses | Wither => 32,
            Fighters | Pointer | Pigscene | BurningSkull | Skeleton | DonkeyKong => 64,
        }
    }

    pub fn height(&self) -> u8 {
        use PaintingMotive::*;

        match self {
            Kebab | Aztec | Alban | Aztec2 | Bomb | Plant | Wasteland | Pool | Courbet | Sea
            | Sunset | Creebet => 16,
            Wanderer | Graham | Match | Bust | Stage | Void | SkullAndRoses | Wither | Fighters => {
                32
            }
            Pointer | Pigscene | BurningSkull | Skeleton | DonkeyKong => 64,
        }
    }

    pub fn center(&self) -> (u8, u8) {
        (self.width() / 2 - 1, self.height() / 2)
    }
}

protocol_enum! {
    enum PaintingDirection: i8 {
        South = 0,
        West = 1,
        North = 2,
        East = 3
    }
}

#[derive(Debug)]
pub enum SculkVibrationDestination {
    Block(Position),
    Entity(VarInt),
}

impl Serialize for SculkVibrationDestination {
    fn serialize(&self, buf: &mut bytes::BytesMut) {
        match self {
            SculkVibrationDestination::Block(pos) => {
                "block".to_string().serialize(buf);
                pos.serialize(buf);
            }
            SculkVibrationDestination::Entity(eid) => {
                "entity".to_string().serialize(buf);
                eid.serialize(buf);
            }
        }
    }

    fn deserialize<B: bytes::Buf>(buf: &mut B) -> crate::error::Res<Self> {
        let s = String::deserialize(buf)?;

        match s.as_str() {
            "block" => Ok(Self::Block(Position::deserialize(buf)?)),
            "entity" => Ok(Self::Entity(VarInt::deserialize(buf)?)),
            _ => Err(ProtocolError::UnknownVariant(
                s,
                "SculkVibrationDestination",
            )),
        }
    }
}

protocol_enum! {
    enum EntityAnimation: u8 {
        SwingMainArm = 0,
        TakeDamage = 1,
        LeaveBed = 2,
        SwingOffhand = 3,
        CriticalEffect = 4,
        MagicCriticalEffect = 5
    }
}

protocol_struct! {
    struct Statistic {
        category: VarInt,
        statistic: VarInt,
        value: VarInt
    }
}

pub enum StatisticCategory {
    Mined(),
    Crafted(),
    Used(),
    Broken(),
    PickedUp(),
    Dropped(),
    Killed(EntityType),
    KilledBy(EntityType),
    Custom(CustomStatistic),
}

protocol_enum! {
    enum CustomStatistic: VarInt {
        LeaveGame = 0,
        PlayOneMinute = 1,
        TimeSinceDeath = 2,
        TimeSinceRest = 3,
        SneakTime = 4,
        WalkOneCm = 5,
        CrouchOneCm = 6,
        SprintOneCm = 7,
        WalkOnWaterOneCm = 8,
        FallOneCm = 9,
        ClimbOneCm = 10,
        FlyOneCm = 11,
        WalkUnderWaterOneCm = 12,
        MinecartOneCm = 13,
        BoatOneCm = 14,
        PigOneCm = 15,
        HorseOneCm = 16,
        AviateOneCm = 17,
        SwimOneCm = 18,
        StriderOneCm = 19,
        Jump = 20,
        Drop = 21,
        DamageDealt = 22,
        DamageDealtAbsorbed = 23,
        DamageDealtResisted = 24,
        DamageTaken = 25,
        DamageBlockedByShield = 26,
        DamageAbsorbed = 27,
        DamageResisted = 28,
        Deaths = 29,
        MobKills = 30,
        AnimalsBred = 31,
        PlayerKills = 32,
        FishCaught = 33,
        TalkedToVillager = 34,
        TradedWithVillager = 35,
        EatCakeSlice = 36,
        FillCauldron = 37,
        UseCauldron = 38,
        CleanArmor = 39,
        CleanBanner = 40,
        CleanShulkerBox = 41,
        InteractWithBrewingstand = 42,
        InteractWithBeacon = 43,
        InspectDropper = 44,
        InspectHopper = 45,
        InspectDispenser = 46,
        PlayNoteblock = 47,
        TuneNoteblock = 48,
        PotFlower = 49,
        TriggerTrappedChest = 50,
        OpenEnderchest = 51,
        EnchantItem = 52,
        PlayRecord = 53,
        InteractWithFurnace = 54,
        InteractWithCraftingTable = 55,
        OpenChest = 56,
        SleepInBed = 57,
        OpenShulkerBox = 58,
        OpenBarrel = 59,
        InteractWithBlastFurnace = 60,
        InteractWithSmoker = 61,
        InteractWithLectern = 62,
        InteractWithCampfire = 63,
        InteractWithCartographyTable = 64,
        InteractWithLoom = 65,
        InteractWithStonecutter = 66,
        BellRing = 67,
        RaidTrigger = 68,
        RaidWin = 69,
        InteractWithAnvil = 70,
        InteractWithGrindstone = 71,
        TargetHit = 72,
        InteractWithSmithingTable = 73
    }
}

protocol_enum! {
    enum DiggingStatus: VarInt {
        StartedDigging = 0,
        CancelledDigging = 1,
        FinishedDigging = 2
    }
}

#[derive(Debug)]
pub enum BossBarAction {
    Add {
        title: Box<Chat>,
        health: f32,
        color: BossBarColor,
        division: BossBarDivision,
        flags: BossBarFlags,
    },
    Remove,
    UpdateHealth(f32),
    UpdateTitle(Box<Chat>),
    UpdateStyle(BossBarColor, BossBarDivision),
    UpdateFlags(BossBarFlags),
}

impl Serialize for BossBarAction {
    fn serialize(&self, buf: &mut bytes::BytesMut) {
        match self {
            Self::Add {
                title,
                health,
                color,
                division,
                flags,
            } => {
                0u8.serialize(buf);
                title.serialize(buf);
                health.serialize(buf);
                color.serialize(buf);
                division.serialize(buf);
                flags.serialize(buf);
            }
            BossBarAction::Remove => 1u8.serialize(buf),
            BossBarAction::UpdateHealth(health) => {
                2u8.serialize(buf);
                health.serialize(buf);
            }
            BossBarAction::UpdateTitle(title) => {
                3u8.serialize(buf);
                title.serialize(buf);
            }
            BossBarAction::UpdateStyle(color, division) => {
                4u8.serialize(buf);
                color.serialize(buf);
                division.serialize(buf);
            }
            BossBarAction::UpdateFlags(flags) => {
                5u8.serialize(buf);
                flags.serialize(buf);
            }
        }
    }

    fn deserialize<B: bytes::Buf>(buf: &mut B) -> crate::error::Res<Self> {
        match VarInt::deserialize(buf)?.0 {
            0 => Ok(Self::Add {
                title: Box::new(Chat::deserialize(buf)?),
                health: f32::deserialize(buf)?,
                color: BossBarColor::deserialize(buf)?,
                division: BossBarDivision::deserialize(buf)?,
                flags: BossBarFlags::deserialize(buf)?,
            }),
            1 => Ok(Self::Remove),
            2 => Ok(Self::UpdateHealth(f32::deserialize(buf)?)),
            3 => Ok(Self::UpdateTitle(Box::new(Chat::deserialize(buf)?))),
            4 => Ok(Self::UpdateStyle(
                BossBarColor::deserialize(buf)?,
                BossBarDivision::deserialize(buf)?,
            )),
            5 => Ok(Self::UpdateFlags(BossBarFlags::deserialize(buf)?)),
            i => Err(ProtocolError::UnknownVariant(
                i.to_string(),
                "BossBarAction",
            )),
        }
    }
}

protocol_enum! {
    enum BossBarColor: VarInt {
        Pink = 0,
        Blue = 1,
        Red = 2,
        Green = 3,
        Yellow = 4,
        Purple = 5,
        White = 6
    }
}

protocol_enum! {
    enum BossBarDivision: VarInt {
        None = 0,
        Six = 1,
        Ten = 2,
        Twelve = 3,
        Twenty = 4
    }
}

bitflags::bitflags! {
    pub struct BossBarFlags: u8 {
        const DARK_SKY = 0x01;
        const DRAGON_BAR = 0x02;
        const FOG = 0x04;
    }
}

impl Serialize for BossBarFlags {
    fn serialize(&self, buf: &mut bytes::BytesMut) {
        self.bits().serialize(buf);
    }

    fn deserialize<B: bytes::Buf>(buf: &mut B) -> crate::error::Res<Self> {
        Ok(Self::from_bits_truncate(u8::deserialize(buf)?))
    }
}

protocol_enum! {
    enum Difficulty: u8 {
        Peaceful = 0,
        Easy = 1,
        Normal = 2,
        Hard = 3
    }
}

protocol_enum! {
    enum MessagePosition: i8 {
        Chat = 0,
        System = 1,
        GameInfo = 2
    }
}

#[derive(Debug)]
pub struct TabMatch {
    tab_match: String,
    has_tooltip: bool,
    tooltip: Option<Chat>,
}

impl Serialize for TabMatch {
    fn serialize(&self, buf: &mut bytes::BytesMut) {
        self.tab_match.serialize(buf);
        self.has_tooltip.serialize(buf);

        if self.has_tooltip && self.tooltip.is_some() {
            self.tooltip.unwrap().serialize(buf);
        }
    }

    fn deserialize<B: bytes::Buf>(buf: &mut B) -> crate::error::Res<Self> {
        let tab_match = String::deserialize(buf)?;
        let has_tooltip = bool::deserialize(buf)?;

        let tooltip = if has_tooltip {
            Some(Chat::deserialize(buf)?)
        } else {
            None
        };

        Ok(Self {
            tab_match,
            has_tooltip,
            tooltip,
        })
    }
}

#[derive(Debug)]
pub struct Node {
    flags: NodeFlags,
    children: Vec<VarInt>,
    redirect: Option<VarInt>,
    name: Option<String>,
    parser: Option<NodeParser>,
    suggestion_type: Option<String>,
}

impl Serialize for Node {
    fn serialize(&self, buf: &mut bytes::BytesMut) {
        // TODO: Check whether flags and optional values match
        self.flags.serialize(buf);
        self.children.serialize(buf);
        if self.flags.has_redirect() && self.redirect.is_some() {
            self.redirect.unwrap().serialize(buf);
        }
        if (self.flags.is_argument() || self.flags.is_literal()) && self.name.is_some() {
            self.name.unwrap().serialize(buf);
        }
        if self.flags.is_argument() && self.parser.is_some() {
            self.parser.unwrap().serialize(buf);
        }
        if self.flags.has_suggestions() && self.suggestion_type.is_some() {
            self.suggestion_type.unwrap().serialize(buf);
        }
    }

    fn deserialize<B: bytes::Buf>(buf: &mut B) -> crate::error::Res<Self> {
        todo!()
    }
}

#[derive(Debug)]
pub struct NodeFlags(pub u8);

impl NodeFlags {
    pub fn node_type(&self) -> Result<NodeType, ProtocolError> {
        match self.0 & 0x03 {
            0x00 => Ok(NodeType::Root),
            0x01 => Ok(NodeType::Literal),
            0x02 => Ok(NodeType::Argument),
            i => Err(ProtocolError::UnknownVariant(i.to_string(), "NodeType")),
        }
    }

    pub fn is_root(&self) -> bool {
        (self.0 & 0x03) == 0
    }

    pub fn is_literal(&self) -> bool {
        (self.0 & 0x03) == 1
    }

    pub fn is_argument(&self) -> bool {
        (self.0 & 0x03) == 2
    }

    pub fn is_executable(&self) -> bool {
        (self.0 & 0x04) != 0
    }

    pub fn has_redirect(&self) -> bool {
        (self.0 & 0x08) != 0
    }

    pub fn has_suggestions(&self) -> bool {
        (self.0 & 0x10) != 0
    }
}

impl Serialize for NodeFlags {
    fn serialize(&self, buf: &mut bytes::BytesMut) {
        self.0.serialize(buf);
    }

    fn deserialize<B: bytes::Buf>(buf: &mut B) -> crate::error::Res<Self> {
        Ok(Self(u8::deserialize(buf)?))
    }
}

pub enum NodeType {
    Root,
    Literal,
    Argument,
}

#[derive(Debug)]
pub enum NodeParser {}

impl Serialize for NodeParser {
    fn serialize(&self, buf: &mut bytes::BytesMut) {
        todo!()
    }

    fn deserialize<B: bytes::Buf>(buf: &mut B) -> crate::error::Res<Self> {
        todo!()
    }
}
