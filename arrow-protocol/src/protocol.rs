#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Bound {
    Clientbound,
    Serverbound,
}

#[derive(Debug, Clone, Copy)]
pub enum State {
    Handshake,
    Status,
    Login,
    Play,
}
