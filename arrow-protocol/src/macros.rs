macro_rules! protocol {
    (
    $($pversion:literal => $name:ident($ty:ty)),+
    ) => {
        #[derive(Debug)]
        pub enum Protocol {
            Handshake($crate::handshake::Handshake),
            Status($crate::status::Status),
            Login($crate::login::Login),
            $($name($ty)),+
        }

        impl Protocol {
            pub fn serialize(&self, protocol_version: i32) -> bytes::Bytes {
                use bytes::BufMut;
                use $crate::types::Serialize;

                let buf = match self {
                    Self::Handshake(handshake) => handshake.serialize(),
                    Self::Status(status) => status.serialize(),
                    Self::Login(login) => login.serialize(protocol_version),
                    $(Self::$name(play) => play.serialize()),*
                };

                let mut bytes = bytes::BytesMut::new();
                $crate::types::VarInt(buf.len() as i32, 0).serialize(&mut bytes);
                bytes.put(buf);
                bytes.freeze()
            }

            pub fn deserialize(state: $crate::protocol::State, bound: $crate::protocol::Bound, protocol_version: i32, id: i32, mut buf: bytes::Bytes) -> $crate::error::Res<Self> {
                use $crate::protocol::State;

                match state {
                    State::Handshake => if id == 0x0 && bound == $crate::protocol::Bound::Serverbound {
                        $crate::handshake::Handshake::deserialize(&mut buf).map(Self::Handshake) } else { Err($crate::error::ProtocolError::UnknownId(id, state))
                    },
                    State::Status => $crate::status::Status::deserialize(bound, id, buf).map(Self::Status),
                    State::Login => $crate::login::Login::deserialize(bound, protocol_version, id, buf).map(Self::Login),
                    State::Play => match protocol_version {
                        $($pversion => {
                            <$ty>::deserialize(bound, id, buf).map(Self::$name)
                        }),*
                        p => Err($crate::error::ProtocolError::UnsupportedProtocolVersion(p))
                    },
                }
            }
        }
    };
}

macro_rules! protocol_state {
    (
    $name:ident
    clientbound {
        $(
            $cid:literal => $cname:ident {
                $($cfield:ident: $cty:ty),*
            }
        ),*
    },
    serverbound {
        $(
            $sid:literal => $sname:ident {
                $($sfield:ident: $sty:ty),*
            }
        ),*
    }
    ) => {
        #[derive(Debug)]
        pub enum $name {
            $($cname {
                $($cfield : $cty),*
            },)*
            $($sname {
                $($sfield : $sty),*
            }),*
        }

        impl $name {
            pub fn serialize(&self) -> bytes::Bytes {
                use $crate::types::Serialize;
                use bytes::BufMut;

                let mut bytes = bytes::BytesMut::new();

                match self {
                    $(Self::$cname { $($cfield),* } => {
                        let mut buf = bytes::BytesMut::new();

                        $crate::types::VarInt($cid, 0).serialize(&mut buf);
                        $($cfield.serialize(&mut buf);)*

                        $crate::types::VarInt(buf.len() as i32, 0).serialize(&mut bytes);
                        bytes.put(buf);
                    },)*
                    $(Self::$sname { $($sfield),* } => {
                        let mut buf = bytes::BytesMut::new();

                        $crate::types::VarInt($sid, 0).serialize(&mut buf);
                        $($sfield.serialize(&mut buf);)*

                        $crate::types::VarInt(buf.len() as i32, 0).serialize(&mut bytes);
                        bytes.put(buf);
                    }),*
                }

                bytes.freeze()
            }

            pub fn deserialize(bound: $crate::protocol::Bound, id: i32, mut buf: bytes::Bytes) -> $crate::error::Res<Self> {
                use $crate::protocol::Bound;
                use $crate::types::Serialize;

                match bound {
                    Bound::Clientbound => {
                        match id {
                            $($cid => {
                                $(let $cfield = <$cty>::deserialize(&mut buf)?;)*

                                Ok(Self::$cname {
                                    $($cfield),*
                                })
                            }),*
                            _ => panic!()
                        }
                    },
                    Bound::Serverbound => {
                        match id {
                            $($sid => {
                                $(let $sfield = <$sty>::deserialize(&mut buf)?;)*

                                Ok(Self::$sname {
                                    $($sfield),*
                                })
                            }),*
                            _ => panic!()
                        }
                    },
                }
            }
        }
    };
}

macro_rules! packet {
    (
    $id:literal => $name:ident {
        $(
            $field:ident : $ty:ty
        ),*
    }
    ) => {
        #[derive(Debug)]
        pub struct $name {
            $(
                pub $field : $ty
            ),*
        }

        impl $name {
            pub fn serialize(&self) -> bytes::Bytes {
                use $crate::types::Serialize;
                use bytes::BufMut;

                let mut data = bytes::BytesMut::new();

                $crate::types::VarInt($id, 0).serialize(&mut data);
                $(self.$field.serialize(&mut data);)*
                let mut bytes = bytes::BytesMut::new();
                $crate::types::VarInt(data.len() as i32, 0).serialize(&mut bytes);
                bytes.put(data);

                bytes.freeze()
            }

            pub fn deserialize(buf: &mut bytes::Bytes) -> $crate::error::Res<Self> {
                use $crate::types::Serialize;

                $(let $field = <$ty>::deserialize(buf)?;)*

                Ok(Self {
                    $($field),*
                })
            }
        }
    };
}

macro_rules! protocol_enum {
    (enum $name:ident : VarInt { $( $variant:ident = $value:literal ),* }) => {
        #[repr(i32)]
        #[derive(Debug, Clone, Copy)]
        pub enum $name {
            $($variant = $value),*
        }

        impl $crate::types::Serialize for $name {
            fn serialize(&self, buf: &mut bytes::BytesMut) {
                $crate::types::VarInt(*self as i32, 0).serialize(buf);
            }

            fn deserialize<B: bytes::Buf>(buf: &mut B) -> $crate::error::Res<Self> {
                let i = $crate::types::VarInt::deserialize(buf)?.0;

                match i {
                    $($value => Ok(Self::$variant),)*
                    _ => Err($crate::error::ProtocolError::UnknownVariant(i.to_string(), stringify!($name)))
                }
            }
        }
    };
    (enum $name:ident : $ty:ty { $( $variant:ident = $value:literal ),* }) => {
        #[repr($ty)]
        #[derive(Debug, Clone, Copy)]
        pub enum $name {
            $($variant = $value),*
        }

        impl $crate::types::Serialize for $name {
            fn serialize(&self, buf: &mut bytes::BytesMut) {
                (*self as $ty).serialize(buf);
            }

            fn deserialize<B: bytes::Buf>(buf: &mut B) -> $crate::error::Res<Self> {
                let i = <$ty>::deserialize(buf)?;

                match i {
                    $($value => Ok(Self::$variant),)*
                    _ => Err($crate::error::ProtocolError::UnknownVariant(i.to_string(), stringify!($name)))
                }
            }
        }
    };
}

macro_rules! protocol_struct {
    (struct $name:ident { $( $field:ident: $ty:ty ),* }) => {
        #[derive(Debug)]
        pub struct $name {
            $(pub $field: $ty),*
        }

        impl $crate::types::Serialize for $name {
            fn serialize(&self, buf: &mut bytes::BytesMut) {
                $( self.$field.serialize(buf); )*
            }

            fn deserialize<B: bytes::Buf>(buf: &mut B) -> $crate::error::Res<Self> {
                $( let $field = <$ty>::deserialize(buf)?; )*

                Ok(Self { $($field),* })
            }
        }
    };
}
