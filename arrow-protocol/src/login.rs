use bytes::{Bytes, BytesMut};
use uuid::Uuid;

use crate::{
    chat::Chat,
    error::{ProtocolError, Res},
    protocol::{Bound, State},
    types::{NoLengthPrefixVec, Serialize, VarInt},
};

#[derive(Debug)]
pub enum Login {
    Disconnect {
        reason: Box<Chat>,
    },
    EncryptionRequest {
        server_id: String,
        public_key: Vec<u8>,
        verify_token: Vec<u8>,
    },
    LoginSuccess {
        uuid: Uuid,
        name: String,
    },
    SetCompression {
        threshold: VarInt,
    },
    LoginPluginRequest {
        message_id: VarInt,
        // TODO: Identifier type
        channel: String,
        data: NoLengthPrefixVec<u8>,
    },
    LoginStart {
        name: String,
    },
    EncryptionResponse {
        shared_secret: Vec<u8>,
        verify_token: Vec<u8>,
    },
    LoginPluginResponse {
        message_id: VarInt,
        successful: bool,
        data: NoLengthPrefixVec<u8>,
    },
}

impl Login {
    pub fn serialize(&self, protocol_version: i32) -> Bytes {
        let mut bytes = BytesMut::new();

        let offset = match protocol_version {
            385..=390 => 1,
            _ => 0,
        };

        match self {
            Self::Disconnect { reason } => {
                offset.serialize(&mut bytes);
                reason.serialize(&mut bytes)
            }
            Self::EncryptionRequest {
                server_id,
                public_key,
                verify_token,
            } => {
                (0x1u8 + offset).serialize(&mut bytes);
                server_id.serialize(&mut bytes);
                public_key.serialize(&mut bytes);
                verify_token.serialize(&mut bytes);
            }
            Self::LoginSuccess { uuid, name } => {
                (0x2u8 + offset).serialize(&mut bytes);
                if protocol_version >= 707 {
                    uuid.serialize(&mut bytes);
                } else {
                    uuid.to_hyphenated().to_string().serialize(&mut bytes);
                }
                name.serialize(&mut bytes);
            }
            Self::SetCompression { threshold } => {
                (0x3u8 + offset).serialize(&mut bytes);
                threshold.serialize(&mut bytes)
            }
            Self::LoginPluginRequest {
                message_id,
                channel,
                data,
            } => {
                if (385..=390).contains(&protocol_version) {
                    0x0u8.serialize(&mut bytes);
                } else {
                    0x4u8.serialize(&mut bytes);
                }
                message_id.serialize(&mut bytes);
                channel.serialize(&mut bytes);
                data.serialize(&mut bytes);
            }
            Self::LoginStart { name } => {
                offset.serialize(&mut bytes);
                name.serialize(&mut bytes)
            }
            Self::EncryptionResponse {
                shared_secret,
                verify_token,
            } => {
                (0x1u8 + offset).serialize(&mut bytes);
                shared_secret.serialize(&mut bytes);
                verify_token.serialize(&mut bytes);
            }
            Self::LoginPluginResponse {
                message_id,
                successful,
                data,
            } => {
                (0x2u8 + offset).serialize(&mut bytes);
                message_id.serialize(&mut bytes);
                successful.serialize(&mut bytes);
                data.serialize(&mut bytes);
            }
        }

        bytes.freeze()
    }

    pub fn deserialize(bound: Bound, protocol_version: i32, id: i32, mut buf: Bytes) -> Res<Self> {
        Ok(match bound {
            Bound::Clientbound => match protocol_version {
                385..=390 => match id {
                    0x0 => Self::LoginPluginRequest {
                        message_id: VarInt::deserialize(&mut buf)?,
                        channel: String::deserialize(&mut buf)?,
                        data: NoLengthPrefixVec::deserialize(&mut buf)?,
                    },
                    0x1 => Self::Disconnect {
                        reason: Box::new(Chat::deserialize(&mut buf)?),
                    },
                    0x2 => Self::EncryptionRequest {
                        server_id: String::deserialize(&mut buf)?,
                        public_key: Vec::deserialize(&mut buf)?,
                        verify_token: Vec::deserialize(&mut buf)?,
                    },
                    0x3 => Self::LoginSuccess {
                        uuid: Uuid::deserialize(&mut buf)?,
                        name: String::deserialize(&mut buf)?,
                    },
                    0x4 => Self::SetCompression {
                        threshold: VarInt::deserialize(&mut buf)?,
                    },
                    _ => return Err(ProtocolError::UnknownId(id, State::Login)),
                },
                _ => match id {
                    0x0 => Self::Disconnect {
                        reason: Box::new(Chat::deserialize(&mut buf)?),
                    },
                    0x1 => Self::EncryptionRequest {
                        server_id: String::deserialize(&mut buf)?,
                        public_key: Vec::deserialize(&mut buf)?,
                        verify_token: Vec::deserialize(&mut buf)?,
                    },
                    0x2 => Self::LoginSuccess {
                        uuid: Uuid::deserialize(&mut buf)?,
                        name: String::deserialize(&mut buf)?,
                    },
                    0x3 => Self::SetCompression {
                        threshold: VarInt::deserialize(&mut buf)?,
                    },
                    0x4 => Self::LoginPluginRequest {
                        message_id: VarInt::deserialize(&mut buf)?,
                        channel: String::deserialize(&mut buf)?,
                        data: NoLengthPrefixVec::deserialize(&mut buf)?,
                    },
                    _ => return Err(ProtocolError::UnknownId(id, State::Login)),
                },
            },
            Bound::Serverbound => match protocol_version {
                385..=390 => match id {
                    0x0 => Self::LoginPluginResponse {
                        message_id: VarInt::deserialize(&mut buf)?,
                        successful: bool::deserialize(&mut buf)?,
                        data: NoLengthPrefixVec::deserialize(&mut buf)?,
                    },
                    0x1 => Self::LoginStart {
                        name: String::deserialize(&mut buf)?,
                    },
                    0x2 => Self::EncryptionResponse {
                        shared_secret: Vec::deserialize(&mut buf)?,
                        verify_token: Vec::deserialize(&mut buf)?,
                    },
                    _ => return Err(ProtocolError::UnknownId(id, State::Login)),
                },
                _ => match id {
                    0x0 => Self::LoginStart {
                        name: String::deserialize(&mut buf)?,
                    },
                    0x1 => Self::EncryptionResponse {
                        shared_secret: Vec::deserialize(&mut buf)?,
                        verify_token: Vec::deserialize(&mut buf)?,
                    },
                    0x2 => Self::LoginPluginResponse {
                        message_id: VarInt::deserialize(&mut buf)?,
                        successful: bool::deserialize(&mut buf)?,
                        data: NoLengthPrefixVec::deserialize(&mut buf)?,
                    },
                    _ => return Err(ProtocolError::UnknownId(id, State::Login)),
                },
            },
        })
    }
}
