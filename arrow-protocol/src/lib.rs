#[macro_use]
pub(crate) mod macros;
pub mod chat;
pub mod error;
pub mod handshake;
pub mod login;
pub mod protocol;
pub mod status;
pub mod types;
pub mod v1_18_2;

protocol! {
    758 => V1_18_2(v1_18_2::V1_18_2)
}
