protocol_state! {
    Status
    clientbound {
        0x0 => Response { response: String },
        0x1 => Pong { payload: i64 }
    },
    serverbound {
        0x0 => Request {},
        0x1 => Ping { payload: i64 }
    }
}
