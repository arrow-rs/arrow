use std::io;

use arrow_protocol::error::ProtocolError;

pub enum CodecError {
    ProtocolError(ProtocolError), 
    IoError(io::Error),
}

impl From<io::Error> for CodecError {
    fn from(e: io::Error) -> Self {
        Self::IoError(e)
    }
}
