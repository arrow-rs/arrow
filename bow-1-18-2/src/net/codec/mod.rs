pub mod error;

use arrow_protocol::{
    error::ProtocolError,
    protocol::{Bound, State},
    types::{Serialize, VarInt},
    Protocol,
};
use bytes::{Buf, BufMut};
use tokio_util::codec::{Decoder, Encoder};

use self::error::CodecError;

pub struct Codec {
    state: State,
    bound: Bound,
    version: i32,
}

impl Codec {
    pub fn new(bound: Bound, version: i32) -> Self {
        Self {
            state: State::Handshake,
            bound,
            version,
        }
    }

    pub fn version(&self) -> i32 {
        self.version
    }

    pub fn set_state(&mut self, state: State) {
        self.state = state;
    }

    pub fn set_version(&mut self, version: i32) {
        self.version = version
    }
}

macro_rules! unwrap_ser {
    ($expr:expr) => {
        match $expr {
            Ok(x) => x,
            Err(ProtocolError::UnexpectedEof) => return Ok(None),
            Err(e) => return Err(CodecError::ProtocolError(e)),
        }
    };
}

impl Decoder for Codec {
    type Item = Protocol;

    type Error = CodecError;

    fn decode(&mut self, src: &mut bytes::BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        let len = unwrap_ser!(VarInt::deserialize(src)).0 as usize;

        if src.remaining() >= len {
            let id = unwrap_ser!(VarInt::deserialize(src));

            Ok(Some(unwrap_ser!(Protocol::deserialize(
                self.state,
                self.bound,
                self.version,
                id.0,
                src.copy_to_bytes(len - id.1 as usize),
            ))))
        } else {
            Ok(None)
        }
    }
}

impl Encoder<Protocol> for Codec {
    type Error = CodecError;

    fn encode(&mut self, item: Protocol, dst: &mut bytes::BytesMut) -> Result<(), Self::Error> {
        dst.put(item.serialize(self.version));
        Ok(())
    }
}
