use arrow_protocol::{protocol::State, status::Status, Protocol};
use futures::{SinkExt, StreamExt};
use tokio::net::TcpStream;
use tokio_util::codec::Framed;

use super::codec::Codec;

pub async fn status(mut framed: Framed<TcpStream, Codec>) {
    framed.codec_mut().set_state(State::Status);

    if let Some(Ok(Protocol::Status(Status::Request {}))) = framed.next().await {
        if framed
            .send(Protocol::Status(Status::Response {
                response: r#"{
                        "version": {
                            "name": "1.18.2",
                            "protocol": 758
                        },
                        "players": {
                            "max": 100,
                            "online": 0
                        },
                        "description": {
                            "text": "Hello world"
                        }
                    }"#
                .to_string(),
            }))
            .await
            .is_err()
        {
            return;
        }

        if let Some(Ok(Protocol::Status(Status::Ping { payload }))) = framed.next().await {
            if framed
                .send(Protocol::Status(Status::Pong { payload }))
                .await
                .is_err()
            {}
        }
    }
}
