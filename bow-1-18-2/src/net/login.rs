use arrow_protocol::{login::Login, Protocol, protocol::State};
use flume::Sender;
use futures::{SinkExt, StreamExt};
use tokio::net::TcpStream;
use tokio_util::codec::Framed;

use crate::{
    main_loop::ToMainLoop,
    player::{offline_uuid, Player},
};

use super::codec::Codec;

pub async fn login(mut framed: Framed<TcpStream, Codec>, send: Sender<ToMainLoop>) {
    if framed.codec().version() < 757 {
        
    } else if framed.codec().version() > 757 {
        
    }

    framed.codec_mut().set_state(State::Login);

    if let Some(Ok(Protocol::Login(Login::LoginStart { name }))) = framed.next().await {
        send.send_async(ToMainLoop::IsLoggedIn(name.clone()))
            .await
            .unwrap();

        let uuid = offline_uuid(&name);

        if let Ok(_) = framed
            .send(Protocol::Login(Login::LoginSuccess {
                name: name.clone(),
                uuid,
            }))
            .await
        {
            let player = Player::new(framed, name, uuid);

            send.send_async(ToMainLoop::AddPlayer(player)).await.unwrap();
        }
    }
}
