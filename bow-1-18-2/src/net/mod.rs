pub mod codec;
pub mod login;
pub mod status;

use arrow_protocol::{protocol::Bound, Protocol};
use bytes::{BufMut, BytesMut};
use flume::{Receiver, Sender};
use futures::StreamExt;
use tokio::{
    io::{self, AsyncWriteExt},
    net::TcpListener,
    select,
};
use tokio_util::codec::Framed;

use crate::{
    main_loop::{ServerHandle, ToMainLoop},
    net::{codec::Codec, login::login, status::status},
};

pub enum ToNetLoop {
    Stop,
}

pub async fn spawn_net_loop(server: ServerHandle, recv: Receiver<ToNetLoop>) {
    match net_loop(server.channel, recv).await {
        Ok(()) => {}
        Err(err) => {
            eprintln!("Error: {err}.");
        }
    };
}

async fn net_loop(send: Sender<ToMainLoop>, recv: Receiver<ToNetLoop>) -> Result<(), io::Error> {
    let listener = TcpListener::bind(("0.0.0.0", 25565)).await?;

    loop {
        select! {
            val = listener.accept() => {
                let send = send.clone();

                tokio::spawn(async move {
                    if val.is_err() {
                        return;
                    }

                    let mut stream = val.unwrap().0;

                    let mut buf = [0; 2];
                    if stream.peek(&mut buf).await.is_err() {
                        return
                    }

                    if buf == [0xfe, 0x01] {
                        let mut packet: BytesMut = BytesMut::new();
                        let string = "§1\0127\01.18.2\0Hello World\00\020";

                        packet.put_u8(0xff);
                        packet.put_u16(string.chars().count() as u16);
                        packet.put_slice(
                            string
                                .encode_utf16()
                                .map(|c| c.to_be_bytes())
                                .flatten()
                                .collect::<Vec<_>>()
                                .as_slice(),
                        );

                        if stream.write_all(&packet).await.is_err() {
                            return;
                        }

                        return;
                    }

                    let mut framed = Framed::new(stream, Codec::new(Bound::Serverbound, 0));

                    match framed.next().await {
                        Some(Ok(Protocol::Handshake(handshake))) => {
                            framed.codec_mut().set_version(handshake.version.0);

                            match handshake.next_state.0 {
                                0x1 => status(framed).await,
                                0x2 => login(framed, send).await,
                                _ => return,
                            }
                        }
                        _ => {}
                    }
                });
            },
            val = recv.recv_async() => {
                match val {
                    Ok(ToNetLoop::Stop) => break,
                    Err(_) => break,
                }
            }
        };
    }

    Ok(())
}
