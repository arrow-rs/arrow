use std::time::Duration;

use flume::{Receiver, Sender};
use tokio::{select, time::interval};
use uuid::Uuid;

use crate::{
    net::ToNetLoop,
    player::{offline_uuid, Player},
};

#[derive(Clone, Debug)]
pub struct ServerHandle {
    pub channel: Sender<ToMainLoop>,
}

pub enum ToMainLoop {
    IsLoggedIn(String),
    AddPlayer(Player),
}

pub fn spawn_main_loop() -> (ServerHandle, Receiver<ToNetLoop>) {
    let (send, recv) = flume::unbounded();
    let (net_send, net_recv) = flume::unbounded();

    let handle = ServerHandle { channel: send };

    tokio::spawn(main_loop(recv, net_send));

    (handle, net_recv)
}

async fn main_loop(recv: Receiver<ToMainLoop>, mut _net_send: Sender<ToNetLoop>) {
    let mut players: Vec<(Uuid, Player)> = vec![];

    let mut interval = interval(Duration::from_millis(250));

    loop {
        select! {
            msg = recv.recv_async() => match msg.unwrap() {
                ToMainLoop::IsLoggedIn(name) => {
                    let uuid = offline_uuid(&name);

                    let mut idx = None;

                    for (i, (u, player)) in players.iter_mut().enumerate() {
                        if *u == uuid {
                            player.kick("You logged in from another location").await;
                            idx = Some(i);
                        }
                    }

                    if let Some(i) = idx {
                        players.remove(i);
                    }
                }
                ToMainLoop::AddPlayer(player) => {
                    players.push((player.uuid().clone(), player));
                }
            },
            _ = interval.tick() => {
                for (_, player) in &mut players {
                    player.recv().await;
                }
            }
        }
    }
}
