use futures::StreamExt;
use md5::{Digest, Md5};
use tokio::net::TcpStream;
use tokio_util::codec::Framed;
use uuid::Uuid;

use crate::net::codec::Codec;

pub struct Player {
    framed: Framed<TcpStream, Codec>,
    name: String,
    uuid: Uuid,
}

impl Player {
    pub fn new(framed: Framed<TcpStream, Codec>, name: String, uuid: Uuid) -> Self {
        Self { framed, name, uuid }
    }

    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    pub fn uuid(&self) -> Uuid {
        self.uuid
    }

    pub async fn kick(&mut self, reason: &str) {

    }

    pub async fn recv(&mut self) {
        while let Some(packet) = self.framed.next().await {
            match packet {
                Ok(_) => {},
                Err(e) => {},
            }
        }
    }
}

pub fn offline_uuid(name: &str) -> Uuid {
    let mut hasher = Md5::new();
    hasher.update(format!("OfflinePlayer:{name}"));
    let mut bytes = hasher.finalize();

    bytes[6] = bytes[6] & 0x0f | 0x30;
    bytes[8] = bytes[8] & 0x3f | 0x80;

    Uuid::from_bytes(bytes.into())
}
