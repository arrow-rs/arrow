mod main_loop;
mod net;
mod player;

#[tokio::main]
async fn main() {
    let (handle, recv) = main_loop::spawn_main_loop();
    net::spawn_net_loop(handle.clone(), recv).await;
}
